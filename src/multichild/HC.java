package multichild;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.NodeId;

/**
 * At nodes with node data (only), HC prints the cumulative simplex
 iteration count after solving the node LP.
 *
 * @author Paul A. Rubin (http://about.me/paul.a.rubin)
 */
public class HC extends IloCplex.HeuristicCallback {

  /**
   * Check the node id and if necessary print the cumulative iteration count.
   * @throws IloException if CPLEX balks
   */
  @Override
  protected final void main() throws IloException {
    Object raw = getNodeData();
    if (raw != null && raw instanceof BranchInfo) {
      // get the node id
      NodeId id = ((BranchInfo) raw).getId();
      // get the current simplex iteration count
      long count = getNiterations64();
      System.out.println("::: At node " + id
                         + " (heuristic callback),"
                         + " the simplex iteration count = "
                         + count);
      System.out.flush();
    }
  }

}
