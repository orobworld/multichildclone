/*
 * BranchInfo defines an object that can be attached to nodes and guides
 * branching decisions.
 */
package multichild;

import ilog.cplex.IloCplex.NodeId;

/**
 * @author Paul A. Rubin (http://about.me/paul.a.rubin)
 */
public class BranchInfo {
  private final int minTeams;  // minimum # of teams to use in all child nodes
  private final int maxTeams;  // maximum # of teams to use in all child nodes
  private NodeId id;     // ID of the node

  /**
   * Constructor.
   * @param min minimum number of teams to be used
   * @param max maximum number of teams to be used
   */
  public BranchInfo(final int min, final int max) {
    this.minTeams = min;
    this.maxTeams = max;
  }

  /**
   * Get the maximum number of teams to create among child nodes.
   * @return maximum number of teams to create among child nodes
   */
  public final int getMaxTeams() {
    return maxTeams;
  }

  /**
   * Get the minimum number of teams to create among child nodes.
   * @return minimum number of teams to create among child nodes
   */
  public final int getMinTeams() {
    return minTeams;
  }

  /**
   * Get the node ID from the data.
   * @return the node ID
   */
  public final NodeId getId() {
    return id;
  }

  /**
   * Set the node ID.
   * @param n the node ID to use
   */
  public final void setId(final NodeId n) {
    this.id = n;
  }

}
