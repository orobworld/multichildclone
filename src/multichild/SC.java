package multichild;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;

/**
 * At nodes with node data (only), SC prints the cumulative simplex
 * iteration count before solving the node LP.
 *
 * @author Paul A. Rubin (http://about.me/paul.a.rubin)
 */
public class SC extends IloCplex.SolveCallback {

  /**
   * Check the node id and if necessary print the cumulative iteration count.
   * Then let CPLEX solve the node LP as normal.
   * @throws IloException if CPLEX balks
   */
  @Override
  protected final void main() throws IloException {
    Object raw = getNodeData();
    if (raw != null && raw instanceof BranchInfo) {
      // get the node id
      IloCplex.NodeId id = ((BranchInfo) raw).getId();
      // get the current simplex iteration count
      long count = getNiterations64();
      System.out.println("$$$ At node " + id
                         + " (entering solve callback),"
                         + " the simplex iteration count = "
                         + count);
      System.out.flush();
    }
//    solve(); // uncommenting this mucks up the iteration counts
  }

}
